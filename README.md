# Curso Cypress Intermediário

[Curso intermediário de automação de testes com Cypress](http://talkingabouttesting.coursify.me/courses/testes-automatizados-com-cypress-intermediario) da Escola Talking About Testing.

## Instalação

1. Suba o container do GitLab (isso mesmo! 😉) 

```bash
docker run --publish 80:80 --publish 22:22 --hostname localhost wlsf82/gitlab-ce
```

2. Clone o projeto

```bash
git clone git@gitlab.com:iagofrota/curso-cypress-intermediario.git
```

ou

```bash
git clone https://gitlab.com/iagofrota/curso-cypress-intermediario.git
```

3. Instale os pacotes necessários

```bash
npm install
```

4. [Adicione sua chave SSH no GitLab que você subiu](https://iagofrota.medium.com/gerar-uma-nova-chave-ssh-no-windows-iago-frota-projeto-pessoal-39fba45759ce). 
   Clique em Avatar do seu usuário > _Settings_ > _Access Tokens_. No campo nome, digite o valor 
   curso-cypress-intermediario, na seção Scopes marque a opção 'api', e então clique no botão 'Create personal access 
   token' (by [Walmyr Lima e Silva Filho](https://www.linkedin.com/in/walmyr-lima-e-silva-filho-147a9110a/)).
   

5. Pronto! Agora rode o cypress em modo handless

```bash
npm run test 
```
